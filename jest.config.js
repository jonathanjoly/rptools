module.exports = {
  //preset: 'ts-jest',
  globals: {
    'ts-jest': {
      "compilerHost": true,
    }
  },
  transform: {
    "\\.(ts)$": "ts-jest"
  },
};