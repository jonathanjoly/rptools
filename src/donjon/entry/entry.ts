
import { Elements } from "../enums";
import { randomArea, randomAreas, lastArea } from '../area';
import { createRoom } from "../room";

import { launchDice } from '../../dice'
import { modifyArea } from "../helpers";




export function createEntry() {
    return ENTRIES[launchDice(ENTRIES.length) - 1]();
}



const ENTRIES = [
    function () {
        const room = createRoom(14, 14, { top: Elements.PASSAGE, right: Elements.PASSAGE, bottom: Elements.PASSAGE, left: Elements.PASSAGE });
        const area = randomArea();
        return modifyArea(room, area, Elements.ENTRANCE);
    },
    function () {
        const areas = randomAreas(2);
        const room = createRoom(14, 14, { [areas[0]]: Elements.DOOR, [areas[1]]: Elements.DOOR });
        const area = randomArea(areas);
        return modifyArea(room, area, Elements.ENTRANCE);
    }
    ,
    function () {
        const areas = randomAreas(3);
        const room = createRoom(28, 28, { [areas[0]]: Elements.DOOR, [areas[1]]: Elements.DOOR, [areas[2]]: Elements.DOOR });
        const area = lastArea(areas);
        return modifyArea(room, area, Elements.ENTRANCE);
    }
];