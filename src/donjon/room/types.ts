import { Elements } from "../enums";

export interface RoomOptions {
    top?: Elements;
    right?: Elements;
    bottom?: Elements;
    left?: Elements;

}