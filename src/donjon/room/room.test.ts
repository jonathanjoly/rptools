import { createRoom } from './room';
import { RoomOptions } from './types';
import { Elements } from '../enums';


describe('Test Room', () => {

    describe('createRoom', () => {

        test('Full wall', () => {
            const width = 6;
            const height = 6;
            const roomOptions: RoomOptions = { top: Elements.PASSAGE, right: Elements.PASSAGE, bottom: Elements.PASSAGE, left: Elements.PASSAGE };
            const expected = [
                [Elements.WALL, Elements.WALL, Elements.PASSAGE, Elements.PASSAGE, Elements.WALL, Elements.WALL],
                [Elements.WALL, Elements.FLOOR, Elements.FLOOR, Elements.FLOOR, Elements.FLOOR, Elements.WALL],
                [Elements.PASSAGE, Elements.FLOOR, Elements.FLOOR, Elements.FLOOR, Elements.FLOOR, Elements.PASSAGE],
                [Elements.PASSAGE, Elements.FLOOR, Elements.FLOOR, Elements.FLOOR, Elements.FLOOR, Elements.PASSAGE],
                [Elements.WALL, Elements.FLOOR, Elements.FLOOR, Elements.FLOOR, Elements.FLOOR, Elements.WALL],
                [Elements.WALL, Elements.WALL, Elements.PASSAGE, Elements.PASSAGE, Elements.WALL, Elements.WALL]
            ];
            const resultat = createRoom(width, height, roomOptions);
        });
    });
});