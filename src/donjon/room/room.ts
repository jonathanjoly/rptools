import { Elements } from "../enums";
import { createHorizontalRoomPart } from "../helpers";
import { RoomOptions } from "./types"

export function createSideWall(height: number, option?) {
    if (option) {
        return createHorizontalRoomPart(height, Elements.WALL, { middle: option });
    }
    return createHorizontalRoomPart(height, Elements.WALL);
}

export function createRoom(width: number, height: number, roomOptions: RoomOptions): string[][] {
    const array: string[][] = [];
    const middleHeight = height / 2;

    array[0] = createSideWall(height, roomOptions.top);
    array[height - 1] = createSideWall(height, roomOptions.bottom);

    for (let i = 1; i < height - 1; i++) {
        array[i] = createHorizontalRoomPart(width, Elements.FLOOR, { start: Elements.WALL, end: Elements.WALL });
    }

    if (roomOptions.left) {
        array[middleHeight - 1][0] = roomOptions.left;
        array[middleHeight][0] = roomOptions.left;
    }

    if (roomOptions.right) {
        const endWidtht = height - 1;
        array[middleHeight - 1][endWidtht] = roomOptions.right;
        array[middleHeight][endWidtht] = roomOptions.right;
    }
    return array;
}