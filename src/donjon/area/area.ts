import { Area } from './enums'
import { getRandomELementFomArray, removeValues } from "../helpers"

const AREAS: Area[] = [Area.TOP, Area.RIGHT, Area.BOTTOM, Area.LEFT];

export function randomArea(alreadyUsedArea: Area[] = []): Area {

    if (alreadyUsedArea && alreadyUsedArea.length > 0) {
        return getRandomELementFomArray(removeValues(AREAS, alreadyUsedArea))

    }

    return getRandomELementFomArray(AREAS)
}

export function randomAreas(numberOfAreas: number = 1): Area[] {
    return new Array(numberOfAreas).map((item, i, array) => {
        return randomArea(array);
    })
}

export function lastArea(alreadyUsedArea: Area[] = []): Area {
    return removeValues(AREAS, alreadyUsedArea)[0];
}