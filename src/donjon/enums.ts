export enum Elements {
    WALL = 'W',
    FLOOR = 'F',
    PASSAGE = "P",
    DOOR = 'D',
    ENTRANCE = 'E'
}