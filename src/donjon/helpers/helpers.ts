import { Elements } from "../enums";
import { VerticalMapOption } from './types';
import { launchDice } from '../../dice';
import { Area } from "../area";





export function filledArray(size: number, value: string): string[] {
    return new Array(size).fill(value);
}

export function createHorizontalRoomPart(size: number, fillValue: Elements, additionalOptions?: VerticalMapOption): string[] {
    const part: string[] = filledArray(size, fillValue);

    if (!additionalOptions) {
        return part
    }

    if (additionalOptions.start) {
        part[0] = additionalOptions.start
    }

    if (additionalOptions.middle) {
        const middle = size / 2;
        part[middle - 1] = additionalOptions.middle;
        part[middle] = additionalOptions.middle;
    }

    if (additionalOptions.end) {
        part[size - 1] = additionalOptions.end
    }

    return part;
}

export function modifyTop(based: string[][], value: Elements): string[][] {
    const middle: number = based[0].length / 2;
    based[0][middle - 1] = value;
    based[0][middle] = value;
    return based;
}

export function modifyBottom(based: string[][], value: Elements): string[][] {
    const middle: number = based[0].length / 2;
    const end = based.length - 1;
    based[end][middle - 1] = value;
    based[end][middle] = value;
    return based;
}

export function modifyRight(based: string[][], value: Elements): string[][] {
    const end: number = based[0].length - 1;
    const middle: number = based.length / 2;

    based[middle - 1][end] = value;
    based[middle][end] = value;
    return based;
}

export function modifyLeft(based: string[][], value: Elements): string[][] {
    const middle: number = based.length / 2;
    based[middle - 1][0] = value;
    based[middle][0] = value;
    return based;
}

export function modifyArea(based: string[][], area: Area, value: Elements): string[][] {
    const modificationFunction = {
        [Area.TOP]: modifyTop,
        [Area.RIGHT]: modifyRight,
        [Area.BOTTOM]: modifyBottom,
        [Area.LEFT]: modifyLeft
    }

    return modificationFunction[area](based, value);
}

export function getRandomELementFomArray<T>(array: T[]): T {
    return array[launchDice(array.length) - 1];
}

export function removeValues<T>(items: T[], values: T[]): T[] {
    return items.filter(item => !values.find(value => value === item));
}