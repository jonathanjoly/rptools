import { createHorizontalRoomPart, removeValues } from './index';
import { Elements } from "../enums";


describe('Test Donjon', () => {

    describe('createHorizontalRoomPart', () => {

        test('Full wall', () => {
            const size = 4;
            const fillValue = Elements.WALL
            const additionalOptions = null;
            const expected = [Elements.WALL, Elements.WALL, Elements.WALL, Elements.WALL];
            const resulat = createHorizontalRoomPart(size, fillValue, additionalOptions);
            expect(resulat).toStrictEqual(expected);
        });

        test('Wall and floor', () => {
            const size = 4;
            const fillValue = Elements.FLOOR
            const additionalOptions = { start: Elements.WALL, end: Elements.WALL };
            const expected = [Elements.WALL, Elements.FLOOR, Elements.FLOOR, Elements.WALL];
            const resulat = createHorizontalRoomPart(size, fillValue, additionalOptions);
            expect(resulat).toStrictEqual(expected);
        });

        test('Passage top, wall bottom and floor', () => {
            const size = 4;
            const fillValue = Elements.FLOOR
            const additionalOptions = { start: Elements.PASSAGE, end: Elements.WALL };
            const expected = [Elements.PASSAGE, Elements.FLOOR, Elements.FLOOR, Elements.WALL];
            const resulat = createHorizontalRoomPart(size, fillValue, additionalOptions);
            expect(resulat).toStrictEqual(expected);
        });

        test('Door on middle and wall', () => {
            const size = 4;
            const fillValue = Elements.WALL;
            const additionalOptions = { middle: Elements.DOOR };
            const expected = [Elements.WALL, Elements.DOOR, Elements.DOOR, Elements.WALL];
            const resulat = createHorizontalRoomPart(size, fillValue, additionalOptions);
            expect(resulat).toStrictEqual(expected);
        });


    });
    describe('removeValues', () => {
        test('Basic case', () => {
            const array = ['a', 'b', 'c', 'd', 'e'];
            const values = ['c', 'a', 'e'];
            const expected = ['b', 'd'];
            const result = removeValues(array, values);
            expect(result).toEqual(expected);
        });
        test('Empty array case', () => {
            const array = [];
            const values = ['c', 'a', 'e'];
            const expected = [];
            const result = removeValues(array, values);
            expect(result).toEqual(expected);
        });
        test('Empty values case', () => {
            const array = ['a', 'b', 'c', 'd', 'e'];
            const values = [];
            const expected = ['a', 'b', 'c', 'd', 'e'];
            const result = removeValues(array, values);
            expect(result).toEqual(expected);
        });
    });

})