import { Elements } from "../enums";

export interface VerticalMapOption {
    start?: Elements;
    middle?: Elements;
    end?: Elements;
}