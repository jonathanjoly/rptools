import { DiceConstructable } from "./DiceInterface";

export interface DiceLaunchInterface {
    getValue: () => number;
    roll: () => number;
    reroll: () => number;
}

export interface DiceLaunchConstructable {
    new(Dice: DiceConstructable, launch: string): DiceLaunchInterface;
}

