export interface DiceInterface {
    roll: () => number;
    getValue: () => number;
    getFaces: () => number;
};

export interface DiceConstructable {
    new(faces: number): DiceInterface;
}