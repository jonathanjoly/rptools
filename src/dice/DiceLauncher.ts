import { DiceConstructable } from "./DiceInterface";
import { DiceLaunchConstructable, DiceLaunchInterface } from "./DiceLaunchInterface";

export default class DiceLauncher {

    private DiceLaunch: DiceLaunchConstructable;
    private Dice: DiceConstructable;

    constructor(Dice: DiceConstructable, DiceLaunch: DiceLaunchConstructable) {
        this.Dice = Dice;
        this.DiceLaunch = DiceLaunch;
    }

    createLaunch(launch: string): DiceLaunchInterface {
        return new this.DiceLaunch(this.Dice, launch);
    }

}