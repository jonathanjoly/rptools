import { DiceInterface } from "./DiceInterface";

export default class Dice implements DiceInterface {

    private faces: number;
    private value: number;

    constructor(faces: number) {
        this.faces = faces;
    }

    getFaces(): number {
        return this.faces;
    }

    getValue(): number {
        return this.value;
    }

    roll(): number {
        this.value = Math.round(Math.random() * (this.faces - 1)) + 1;
        return this.getValue();
    }

}