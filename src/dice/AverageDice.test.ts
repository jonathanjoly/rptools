import AverageDice from "./AverageDice";


function testRoll(parameter, expected) {
    test(`Value with value ${parameter}`, () => {
        const dice = new AverageDice(parameter);
        const resultat = dice.roll();
        expect(resultat).toEqual(expected);
        expect(resultat).toBeLessThanOrEqual(parameter);
        expect(dice.getValue()).toEqual(expected);
        expect(dice.getFaces()).toEqual(parameter);
    });
}

describe('AverageDice Class', () => {
    describe('Testing roll function', () => {
        testRoll(10, 6);
        testRoll(3, 2);
    });
});