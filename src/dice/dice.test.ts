import Dice from "./Dice";


describe('Dice Class', () => {
    describe('Testing roll function', () => {
        test('Value with value 10', () => {
            const parameter = 10;
            const dice = new Dice(parameter);
            const resultat = dice.roll();
            expect(resultat).toBeGreaterThan(0);
            expect(resultat).toBeLessThanOrEqual(parameter);
            expect(dice.getValue()).toEqual(resultat);
            expect(dice.getFaces()).toEqual(parameter);
        });
    });
});
