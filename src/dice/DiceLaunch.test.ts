import AverageDice from "./AverageDice";
import DiceLaunch from "./DiceLaunch";

function testRoll(parameter, expected) {
    test(`${parameter}`, () => {

        const launch = new DiceLaunch(AverageDice, parameter);
        const res = launch.roll();
        expect(res).toBe(expected);
    });
}

describe('AverageDice Class', () => {
    describe('Testing roll function', () => {
        testRoll('1d10', 6);
        testRoll('d10', 6);
        testRoll('2d10', 12);
        testRoll('2d10+5', 17);
        testRoll('2d10+2d6', 20);
        testRoll('2d10+2d6+5', 25);
        testRoll('2d10+3+2d6+5', 28);
        testRoll('2d10-5', 7);
        testRoll('2d10-1d4', 9);
        testRoll('2d10-1d4-5', 4);
        testRoll('2d10+1d4-5', 10);
        testRoll('2d10-1d4+5', 14);
        testRoll('1d10/2', 3);
        testRoll('1d10*2', 12);
        testRoll('3d10*2/1d4+5d4', 27);
        testRoll('3d10*2+3/1d4+10-5d4', 8);
    });
});