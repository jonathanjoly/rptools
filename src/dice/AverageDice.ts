import { DiceInterface } from './DiceInterface';

export default class AverageDice implements DiceInterface {

    private faces: number;
    private value: number;

    constructor(faces: number) {
        this.faces = faces;
    }

    getFaces(): number {
        return this.faces;
    }

    getValue(): number {
        return this.value;
    }

    roll(): number {
        this.value = Math.floor(this.faces / 2) + 1;
        return this.getValue();
    }

}