import { DiceConstructable, DiceInterface } from "./DiceInterface";
import { DiceLaunchInterface } from "./DiceLaunchInterface";

export default class DiceLaunch implements DiceLaunchInterface {

    private Dice: DiceConstructable;

    private dices: DiceInterface[] = [];
    private operands: string[] = [];
    private operators: string[] = [];

    private operatorFunctions = {
        '+': (a, b) => a + b,
        '-': (a, b) => a - b,
        '*': (a, b) => a * b,
        '/': (a, b) => a / b,
    };

    constructor(Dice: DiceConstructable, launch: string) {
        this.Dice = Dice;
        const operatorsRegex = /\+|\-|\/|\*/g;
        const operators = launch.match(operatorsRegex);
        const operands = launch.split(operatorsRegex);

        if (!operators) {
            this.addElement(launch);
            return;
        }

        this.addElement(operands[0]);

        operators.forEach((operator, index) => {
            this.operators.push(operator);
            this.addElement(operands[index + 1], operator);
        });
    }


    private addDice(number: number, faces: number, operator: string): void {
        const total = number ? number : 1;
        const op = operator === '-' ? operator : '+';

        for (let i = 0; i < total; i++) {
            this.operands.push(`d${this.dices.length}`);
            this.dices.push(new this.Dice(faces));

            if (i < number - 1) {
                this.operators.push(op);
            }
        }
    }

    private addElement(operand: string, operator: string = '+'): void {
        const elements = operand.replace('d', 'D').split('D');


        if (elements.length > 1) {
            return this.addDice(parseInt(elements[0], 10), parseInt(elements[1], 10), operator);
        }

        this.operands.push(elements[0]);

    }


    private getOperandeValue(operand: string): number {

        if (operand.indexOf('d') === -1) {
            return parseInt(operand);
        };
        const index = operand.replace('d', '');
        const dice = this.dices[index];
        return dice.getValue();
    }

    getValue() {
        return this.operands.reduce((value, operand, index) => {
            const opValue = this.getOperandeValue(operand);

            if (index === 0) {
                return opValue;
            }

            return this.operatorFunctions[this.operators[index - 1]](value, opValue);

        }, 0);
    }

    roll(): number {
        this.dices.forEach(dice => {
            dice.roll();
        });
        return this.getValue();
    }

    reroll(values?: number[]): number {
        if (!values) {
            return this.roll();
        }

        this.dices.forEach(dice => {
            if (values.includes(dice.getValue())) {
                dice.roll();
            }
        });

    }

}