import { AverageDice, DiceLaunch, DiceLauncher } from "../../dice";
import Sale from "./sale";


function testgetItemPrice(parameter, expected) {
    test(`With modificator: ${parameter}`, () => {

        const launcher = new DiceLauncher(AverageDice, DiceLaunch);
        const table = new Sale(launcher);
        const res = table.getItemPrice(parameter);
        expect(res).toBe(expected);
    });

}



describe('Table DD5 Sale Class', () => {
    describe('Testing getItemPrice function', () => {
        testgetItemPrice('common', 50);
        testgetItemPrice('uncommon', 250);
        testgetItemPrice('rare', 2500);
        testgetItemPrice('very rare', 12500);
    });
});
