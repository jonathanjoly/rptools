import { DiceLauncher } from "../../dice";
import { Table } from "../../table";
import { RARITY } from "./Rarity";

export default class Sale {

    static RARITY_PRICE_TABLE = {
        [RARITY.COMMON]: {
            basePrice: 100,
            modifier: '+10',
        },
        [RARITY.UNCOMMON]: {
            basePrice: 500,
            modifier: '+0',
        },
        [RARITY.RARE]: {
            basePrice: 5000,
            modifier: '-10',
        },
        [RARITY.VERY_RARE]: {
            basePrice: 50000,
            modifier: '-20',
        }
    }

    static PRICE_MODIFICATOR_TABLE = {
        limit: 100,
        table: {
            '01-20': {
                operator: '/',
                value: 10
            },
            '21-40': {
                operator: '/',
                value: 4
            },
            '41-80': {
                operator: '/',
                value: 2
            },
            '81-90': {
                operator: '*',
                value: 1
            },
            '91-100': {
                operator: '*',
                value: 1.5
            }
        }
    }

    private table: Table;

    constructor(launcher: DiceLauncher) {
        this.table = new Table(Sale.PRICE_MODIFICATOR_TABLE.table, Sale.PRICE_MODIFICATOR_TABLE.limit, launcher);
    }

    getItemPrice(rarity: RARITY): number {
        const { basePrice, modifier } = Sale.RARITY_PRICE_TABLE[rarity];
        const priceModifier = this.table.roll(modifier);

        if (priceModifier.operator === '/') {
            return basePrice / priceModifier.value;
        }

        return basePrice * priceModifier.value;
    }
}