export enum RARITY {
    COMMON = 'common',
    UNCOMMON = 'uncommon',
    RARE = 'rare',
    VERY_RARE = 'very rare',
}

