import { Table } from ".";
import { AverageDice, DiceLaunch, DiceLauncher } from "../dice";



function testRoll(parameter, expected) {
    test(`With modificator: ${parameter}`, () => {

        const tableMap = {
            '01-02': 'a',
            '03-14': 'b',
            '15-25': 'c',
        }
        const launcher = new DiceLauncher(AverageDice, DiceLaunch);
        const table = new Table(tableMap, 25, launcher);
        const res = table.roll(parameter);
        expect(res).toBe(expected);
    });

}

describe('Table Class', () => {
    describe('Testing roll function', () => {
        testRoll(undefined, 'b');
        testRoll('+10', 'c');
        testRoll('-10', 'b');
        testRoll('+20', 'c');
        testRoll('-20', 'a');
    });
});



