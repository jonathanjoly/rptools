import { DiceLauncher } from "../dice";
import { TableMap } from "./TableMap";

export default class Table {

    private tableMap: TableMap;
    private diceLauncher: DiceLauncher;
    private limit: number;

    constructor(tableMap: TableMap, limit: number, diceLauncher: DiceLauncher) {
        this.tableMap = tableMap;
        this.diceLauncher = diceLauncher;
        this.limit = limit;
    }

    private adaptValueInLimits(value: number): number {
        if (value > this.limit) {
            return this.limit;
        }

        if (value < 1) {
            return 1;
        }

        return value;
    }



    private findKey(value: number): string {
        return Object.keys(this.tableMap).find(key => {
            const bond = key.split('-');
            const start = parseInt(bond[0], 10);

            if (bond.length === 1) {
                return start === value;
            }

            return start <= value && value <= parseInt(bond[1], 10);
        });
    }

    roll(modificator?: string) {
        const dice = `1d${this.limit}`;
        const launch = modificator ? `${dice}${modificator}` : dice;
        const launcher = this.diceLauncher.createLaunch(launch);
        const value = this.adaptValueInLimits(launcher.roll());
        const key = this.findKey(value);
        return this.tableMap[key];
    }
}